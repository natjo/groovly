<div align="left"><img src="media/logo.svg" width="160"></div>

# Groovly
Tell us about your favorite artists and we will generate a playlist of undiscovered musicians specially for you.

The code mainly responsible for generating the playlist can be found in `src/spotify/generate_playlist.js`.

## How to use it

#### Run a local development version
```
npm start
```

#### Build a production image
```
./dev/build_prod.sh
```
The production image adds nginx as a webserver
