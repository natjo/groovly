#!/bin/bash

set -e

# Build and push to Gitlab registry
TAG=latest

docker build -f dev/Dockerfile.prod -t registry.gitlab.com/natjo/groovly:$TAG .
echo "docker login registry.gitlab.com"
docker login registry.gitlab.com
docker push registry.gitlab.com/natjo/groovly:$TAG

