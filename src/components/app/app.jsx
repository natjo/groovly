import React from 'react';
import { Grid } from '@material-ui/core';
import Box from '@material-ui/core/Box';
import { makeStyles } from '@material-ui/core/styles';
import theme from 'theme';
import { ThemeProvider } from '@material-ui/styles';
import { Footer } from 'components/footer/footer'
import { Header } from 'components/header/header'
import { ViewLogin, ViewCreate } from 'components';
import { getHashParams, cleanUrl } from 'utils/general';
import {
    saveAccessTokenFromHashParams,
    existsValidSpotifyAccessToken,
    isValidResponse,
    removeExpiredSpotifyAccessToken,
} from 'spotify/auth';


const useStyles = makeStyles({
    root: {
        minHeight: '100vh',
        flexGrow: 1,
    },
});

export function App() {
    const classes = useStyles();
    const response = getHashParams();
    if (isValidResponse(response)) {
        saveAccessTokenFromHashParams(response);
    }
    cleanUrl();
    removeExpiredSpotifyAccessToken();

    function DisplayedApp() {
        if (existsValidSpotifyAccessToken()) {
            return <ViewCreate />;
        } else {
            return <ViewLogin />;
        }
    }

    return (
        <ThemeProvider theme={theme}>
            <Grid container direction='column' justify='space-between' className={classes.root} >
                <Grid item>
                    <Header />
                </Grid>
                <Grid item>
                    <Box py={3} height="100%">
                        <DisplayedApp/>
                    </Box>
                </Grid>
                <Grid item>
                    <Footer />
                </Grid>
            </Grid>
        </ThemeProvider>
    );
}
