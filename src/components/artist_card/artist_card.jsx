import React, { useState } from 'react';
import { Grid, IconButton, Fade, Divider, Box, Paper, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import CloseIcon from '@material-ui/icons/Close';
import { getNumFollowersFormatted } from 'utils/general';

const ANIMATION_TIMEOUT = 750;  // ms

const useStyles = makeStyles(({ palette }) => ({
    card: {
        textAlign: 'center',
        background: `linear-gradient(${palette.secondary.light}, white)`,
        '@media (min-width: 600px)': {
            width: "13vw"
        }
    },
    media: {
        width: '140px',
        '@media (min-width: 600px)': {
            // width: "200px"
            width: "13vw"
        }
    },
    removeIconButton: {
        // color: palette.common.white,
        color: palette.grey[600],
        width: '1.1em',
        height: '1.1em',
    },
}));

export function ArtistCard(props) {
    // props:
    // artist: artist object from api call
    // removePickedArtist: function called on removal action, called with artist object as parameter

    const classes = useStyles();

    const [shown, setShown] = useState(true);

    function Content() {
        return (
            <Box pt={1}>
                <Grid container direction="column" justify="space-evenly" alignItems="stretch">
                    <Typography variant="h5" align="center">
                        <Box fontWeight="fontWeightBold">
                            {props.artist.name}
                        </Box>
                    </Typography>
                    <Box py={1}>
                        <Typography variant="body2" align="center" color="textSecondary">
                            {getNumFollowersFormatted(props.artist)} Followers
                        </Typography>
                    </Box>
                    <Divider light />
                    <Grid item>
                        <IconButton
                            aria-label="delete"
                            onClick={() => {
                                setTimeout(() => {
                                    props.removePickedArtist(props.artist)
                                }, ANIMATION_TIMEOUT)
                                setShown(false);
                            }}
                        >
                            <CloseIcon className={classes.removeIconButton} />
                        </IconButton>
                    </Grid>
                </Grid>
            </Box>
        )
    }


    // Available keys of the artist object:
    // - name
    // - id
    // - followers
    // - popularity
    // - imageUrl
    return (
        <Box px={3} py={1}>
            <Fade in={shown} timeout={ANIMATION_TIMEOUT}>
                <Paper className={classes.card} elevation={3}>
                    <Grid container justify="flex-start" alignItems="center">
                        <Grid container item xs={4} sm={12} justify="center" alignItems="center">
                            <Grid item>
                                <img src={props.artist.imageUrl} alt={props.artist.name} className={classes.media} />
                            </Grid>
                        </Grid>
                        <Grid item xs={8} sm={12}>
                            <Box px={2}>
                                <Content />
                            </Box >
                        </Grid>
                    </Grid>
                </Paper>
            </Fade>
        </Box>
    )
}