import React, { useState } from 'react';
import { Grid, Fade, Box, Paper, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const ANIMATION_TIMEOUT = 750;  // ms

const useStyles = makeStyles(({ palette }) => ({
    card: {
        textAlign: 'center',
        background: `linear-gradient(${palette.secondary.light}, white)`,
        '@media (min-width: 600px)': {
            width: "13vw"
        }
    },
}));

export function EmptyArtistCard(props) {
    const classes = useStyles();
    // eslint-disable-next-line
    const [shown, setShown] = useState(true);

    return (
        <Box px={3} py={1}>
            <Fade in={shown} timeout={ANIMATION_TIMEOUT}>
                <Paper className={classes.card} elevation={3}>
                    <Grid container justify="center" alignItems="center">
                        <Grid item>
                            <Typography variant="body1" align="center" color="textSecondary" component="div">
                                <Box py={4}>
                                    {props.text}
                                </Box>
                            </Typography>
                        </Grid>
                    </Grid>
                </Paper>
            </Fade>
        </Box>
    )
}