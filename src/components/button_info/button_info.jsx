import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Grid, Box, Button, Dialog, DialogContent, IconButton, Typography, Slide, DialogActions, Link, List, ListItem, ListItemText } from '@material-ui/core';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import HelpOutlineIcon from '@material-ui/icons/HelpOutline';
import CloseIcon from '@material-ui/icons/Close';
import constants from 'constants.js';

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

const useStyles = makeStyles((theme) => ({
    button: {
        color: theme.palette.common.white,
        fontSize: "0.7em",
        '@media (max-width: 600px)': {
            fontSize: "0.6em"
        }
    },
    largeIcon: {
        width: '1.8em',
        height: '1.8em',
        '@media (max-width: 600px)': {
            width: '1.2em',
            height: '1.2em',
        }
    },
    dialogRoot: {
        margin: 0,
        padding: theme.spacing(2),
    },
    dialogCloseButton: {
        position: 'absolute',
        right: theme.spacing(1),
        top: theme.spacing(1),
        color: theme.palette.grey[500],
    },
    link: {
        color: theme.palette.primary.dark,
    }
}));

function CustomDialogTitle(props) {
    const classes = useStyles();
    return (
        <MuiDialogTitle className={classes.root}>
            <Grid container justify="space-between" alignItems="center" >
                <Typography variant="h4">{props.children}</Typography>
                {props.onClick ? (
                    <IconButton aria-label="close" className={classes.closeButton} onClick={props.onClick}>
                        <CloseIcon />
                    </IconButton>
                ) : null}
            </Grid>
        </MuiDialogTitle >
    );
};

export function ButtonInfo(props) {
    // Displays info about the website, meant to be part of the header
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);

    function handleClickButton() {
        setOpen(true);
    }

    function handleClose() {
        setOpen(false);
    }


    //    Spotify allows us to discover thousands of talented musicians, yet we repeatedly listen to the same.
    return (
        <div>
            <Button
                onClick={handleClickButton}
                startIcon={<HelpOutlineIcon className={classes.largeIcon} />}
                className={classes.button}
            >
                About
            </Button>
            <Dialog
                open={open}
                fullWidth={true}
                maxWidth={'md'}
                TransitionComponent={Transition}
                keepMounted
                onClose={handleClose}>
                <CustomDialogTitle onClick={handleClose}>About Groovly</CustomDialogTitle>
                <DialogContent dividers>

                    <Typography variant="h5" gutterBottom>
                        What's The Idea?
                    </Typography>
                    <Typography variant="body1" gutterBottom>
                        Spotify created an amazing platform for both musicians and listeners:
                    </Typography>
                    <Typography variant="body1" gutterBottom>
                        On one hand musicians don’t need huge labels anymore to present their work of art to the world.
                        On the other hand, listeners can enjoy nearly all their favorite artists or discover a never-ending stream of new music.
                    </Typography>
                    <Typography variant="body1" gutterBottom>
                        Unfortunately, Spotify´s algorithms, their playlists and their recommendations present us constantly with the same already famous artists.
                    </Typography>
                    <Typography variant="body1" gutterBottom>
                        We want to give you the chance to easily discover new music according right to your taste and especially help to promote unknown, yet promising musicians along the way!
                        Who doesn't like to say, they knew the artist before they got famous?
                    </Typography>

                    <Box mt={4}>
                        <Typography variant="h5" gutterBottom>
                            How Does It Work?
                        </Typography>
                    </Box>
                    <Typography variant="body1" gutterBottom>
                        You tell us your favorite artists, we generate a playlist tailored exclusively to you!
                    </Typography>
                    <Typography variant="body1" gutterBottom>
                        Moreover, you can choose how many songs you want in your new playlist and how known the artists should already be, by telling us how many followers they should have at most.
                    </Typography>
                    <Typography variant="body1" gutterBottom>
                        Don’t worry, few followers don’t mean that they are boring. We select only good, aspiring and matching to your taste musicians!
                    </Typography>

                    <Box mt={4}>
                        <Typography variant="h5" gutterBottom>
                            More Details
                        </Typography>
                    </Box>
                    <Typography variant="body1" gutterBottom>
                        To break it down: Some math and lots of computer stuff. If you are interested in that: Our website is completely open source!
                        Check out the source code <Link className={classes.link} href={constants.SOURCE_CODE_PAGE}>here</Link>!
                    </Typography>
                    <Typography variant="body1" gutterBottom>
                        We rely on the Spotify database to receive artist information.
                        That is why we need you to sign in. On the upside of that, we can additionally add your new playlist automatically to your account, without any additional effort for you!
                    </Typography>
                    <Typography variant="body1" gutterBottom>
                        And don’t worry, everything happens on your device and only your device communicates with Spotify!
                    </Typography>
                    <Box mt={3}>
                        <Typography variant="body1" gutterBottom>
                            So, start right now. Your new favorite playlist is just a few steps away!
                    </Typography>
                    </Box>

                    <Box mt={4}>
                        <Typography variant="h5" gutterBottom>
                            Legal Information
                        </Typography>
                    </Box>
                    <Typography variant="body1">
                        Groovly.app is developed with our consent for the Spotify
                    </Typography>
                    <List disablePadding dense={true}>
                        <ListItem>
                            <ListItemText primary="a) Terms and Conditions of Use" />
                        </ListItem>
                        <ListItem>
                            <ListItemText primary="b) Privacy Policy" />
                        </ListItem>
                        <ListItem>
                            <ListItemText primary="c) Branding Guidelines." />
                        </ListItem>
                    </List>
                    <Typography variant="body1" gutterBottom component="div">
                        It is part of the <Box display="inline" fontStyle="italic">Spotify for Developer Community</Box> and not directly affiliated with Spotify.
                    </Typography>
                    <Typography variant="body1" gutterBottom>
                        Groovly.app is a completely non-profit application. It's only purpose is to help friends and family to discover new music.
                        Therefore, the liability and required information according to § 5 TMG and § 55 RStV is not applicable.
                    </Typography>
                    <Typography variant="body1" gutterBottom>
                        You can always reach us at  <Link className={classes.link} href={"info@groovly.app"}>info@groovly.app</Link> for questions, suggestions or just to say hello!
                    </Typography>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="secondary">
                        Back
                    </Button>
                </DialogActions>
            </Dialog>
        </div >
    );
}
