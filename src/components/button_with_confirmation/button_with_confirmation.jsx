import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogTitle from '@material-ui/core/DialogTitle';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(({ palette }) => ({
    alert: {
        background: palette.alert.main,
    },
}));

export function ButtonWithConfirmation(props) {
    const classes = useStyles();
    // Can be used as a general button
    // Additional props:
    // dialogText, text that will be displayed in the confirmation dialog
    // warning, if flag set, will have warning color
    const [dialogOpen, setDialogOpen] = React.useState(false);

    function handleClickButton() {
        // If a function exists to check if a dialog should be shown and it is false, go directly to the onClick action
        if (props.checkShowDialog === undefined || props.checkShowDialog()) {
            setDialogOpen(true);
        } else {
            props.onClick();
        }
    }

    function handleCloseDialog(accepted) {
        setDialogOpen(false);
        if (accepted) {
            props.onClick();
        }
    }

    function getClasses(){
        if (props.alert){
            return classes.alert;
        }
    }

    return (
        <div>
            <Button onClick={handleClickButton} variant="contained" color="primary" className={getClasses()}>{props.children}</Button>
            <Dialog open={dialogOpen} onClose={() => handleCloseDialog(false)}>
                <DialogTitle>{props.dialogText}</DialogTitle>
                <DialogActions>
                    <Button onClick={() => handleCloseDialog(false)}>Cancel</Button>
                    <Button onClick={() => handleCloseDialog(true)} autoFocus>
                        OK
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}
