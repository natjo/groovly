import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

export function ButtonWithFinishDialog(props) {
    // Can be used as a general button
    // The onClick method must expected a function as a parameter that will be executed on it's finish
    // Additional props:
    // dialogText, text that will be displayed in the finish message dialog
    const [open, setOpen] = React.useState(false);

    function handleClickButton() {
        props.onClick(() => {
            setOpen(true);
        });
    }

    function handleClose() {
        setOpen(false);
    }

    return (
        <div>
            <Button onClick={handleClickButton} variant="contained" color="primary">{props.children}</Button>
            <Dialog open={open} TransitionComponent={Transition} keepMounted onClose={handleClose}>
                <DialogTitle>{props.dialogText}</DialogTitle>
                <DialogActions>
                    <Button onClick={handleClose} variant="contained" color="primary">
                        OK
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}
