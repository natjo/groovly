import React from 'react';
import Box from '@material-ui/core/Box';
import { Grid } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  footer: {
    padding: theme.spacing(3, 2),
    backgroundColor: theme.palette.grey[100],
  },
}));

function Contact() {
  return (
    <div>
      <Typography variant="body1">Contact</Typography>
      <Typography variant="body2" color="textSecondary">
        info@groovly.app
    </Typography>
    </div>
  );
}


function Disclaimer() {
  return (
    <Typography variant="body2" color="textSecondary" align="right">
      {'This website is not affiliated with Spotify'}

    </Typography>
  );
}


export function Footer() {
  const classes = useStyles();

  return (
    <footer className={classes.footer}>
      <Box mx={4}>
        <Grid container className={classes.root}>
          <Grid container item direction="column" justify="center" alignItems="flex-start" xs={6}>
            <Contact />
            {/* <Source /> */}
          </Grid>
          <Grid container item direction="column" justify="center" alignItems="flex-end" xs>
            <Disclaimer />
          </Grid>
        </Grid>
      </Box>
    </footer>
  );
}