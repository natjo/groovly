import React from 'react';
import Box from '@material-ui/core/Box';
import { Grid } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import { ButtonInfo } from 'components/button_info/button_info';
import GroovlyLogo from 'icons/LogoNoBackground.js';

const useStyles = makeStyles((theme) => ({
  header: {
    padding: theme.spacing(3, 2),
    backgroundColor: theme.palette.secondary.main,
  },
  titleText: {
    color: theme.palette.secondary.contrastText,
    '@media (max-width: 600px)': {
      fontSize: 24,
    }
  },
  logo: {
    height: '50px',
    width: '50px',
    '@media (min-width: 600px)': {
      height: '100px',
      width: '100px'
    }
  }
}));


export function Header() {
  const classes = useStyles();

  function GroovlyText() {
    return (
      <Typography variant="h3" className={classes.titleText}>
        <Box fontWeight="fontWeightBold">
          {'Groovly'}
        </Box>
      </Typography>
    );
  }


  function GroovlyLogoWithText() {
    return (
      <Grid container justify="flex-start" alignItems="center" spacing={1} >
        <Grid item>
          <GroovlyLogo alt="Groovly Logo" className={classes.logo} />
        </Grid>
        <Grid item>
          <GroovlyText />
        </Grid>
      </Grid >
    )
  }

  return (
    <footer className={classes.header}>
      <Box mx={1}>
        <Grid container justify="space-between" alignItems="center" spacing={2}>
          <Grid item >
            <GroovlyLogoWithText />
          </Grid>
          <Grid item >
            <Grid container justify="flex-end" alignItems="center" >
              <ButtonInfo />
            </Grid>
          </Grid>
        </Grid>
      </Box>
    </footer >
  );
}