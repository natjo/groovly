import React from 'react';
import TextField from '@material-ui/core/TextField';
import useLocalStorage from "react-use-localstorage";


export function NumericalInput(props) {
    // Textfield that allows only numerical input
    // Props:
    // label: string, will be shown as the label of the field. Will also be used as part of the key for content persistence throught reloads
    // defaultValue: starting value of the field
    // valueRef: reference value that can be used from the parent component. Will be up to date with current value
    let local_storage_label = "numerical_input_" + props.label.replace(/\s/g, '_');
    const [value, setValue] = useLocalStorage(local_storage_label, props.defaultValue);


    function onChange(event) {
        const newValueStr = event.target.value;
        const newValue = parse(newValueStr);
        setValue(newValueStr);
        props.valueRef.current = newValue;
    }

    function parse(valueStr) {
        if (valueStr.length === 0) {
            return 0;
        }
        const parsedValue = parseInt(valueStr);
        if (Number.isNaN(parsedValue)) {
            return props.defaultValue;
        }
        return parsedValue;
    }

    return (
        <TextField
            value={value}
            label={props.label}
            // tel is like number, but without the arrows
            type="tel"
            onChange={onChange}
            variant="outlined"
        />
    );
}
