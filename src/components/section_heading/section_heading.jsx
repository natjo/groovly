import React from 'react';
import Typography from '@material-ui/core/Typography';

export function SectionHeading(props) {
    return (
        <Typography variant="h4" align="center">
            {props.title}
        </Typography>
    );
}
