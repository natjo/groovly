import React, { useState } from 'react';
import { Grid, Paper, Box, Divider, Fade, IconButton, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import CloseIcon from '@material-ui/icons/Close';
import { getNumFollowersFormatted } from 'utils/general';

const ANIMATION_TIMEOUT = 750;  // ms

const useStyles = makeStyles(({ palette }) => ({
    card: {
        textAlign: 'center',
        background: `linear-gradient(${palette.secondary.light}, white)`,
        '@media (min-width: 600px)': {
            width: "10vw"
        }
    },
    media: {
        width: '120px',
        '@media (min-width: 600px)': {
            // width: "200px"
            width: "10vw"
        }
    },
}));

export function SongCard(props) {
    // props:
    // song: song object from api call
    // removeSong: function called on removal action, called with song object as parameter

    const classes = useStyles();

    const [shown, setShown] = useState(true);

    // Available keys of the song object:
    // id
    // name
    // popularity
    // songUri
    // imageUrl
    // artist

    // Available keys of the artist object:
    // - name
    // - id
    // - followers
    // - popularity
    // - imageUrl
    let song = props.song;
    let artist = song.artist;

    function Content() {
        return (
            <Box pt={1}>
                <Grid container direction="column" justify="space-evenly" alignItems="stretch">
                    <Typography variant="body1" align="center">
                        {song.artist.name}
                    </Typography>
                    <Typography variant="h6" align="center">
                        {song.name}
                    </Typography>
                    <Box py={1}>
                        <Typography variant="body2" align="center" color="textSecondary">
                            {getNumFollowersFormatted(artist)} Followers
                        </Typography>
                    </Box>
                    <Divider light />
                    <Grid item>
                        <IconButton
                            aria-label="delete"
                            onClick={() => {
                                setTimeout(() => {
                                    props.removeSong(song)
                                }, ANIMATION_TIMEOUT)
                                setShown(false);
                            }}
                        >
                            <CloseIcon className={classes.removeIconButton} />
                        </IconButton>
                    </Grid>
                </Grid>
            </Box>
        );
    }

    return (
        <Box px={2} py={1}>
            <Fade in={shown} timeout={ANIMATION_TIMEOUT}>
                <Paper className={classes.card} elevation={3}>
                    <Grid container justify="flex-start" alignItems="center">
                        <Grid container item xs={4} sm={12} justify="center" alignItems="center">
                            <Grid item>
                                <img src={song.imageUrl} alt={song.name} className={classes.media} />
                            </Grid>
                        </Grid>
                        <Grid item xs={8} sm={12}>
                            <Box px={2}>
                                <Content />
                            </Box>
                        </Grid>
                    </Grid>
                </Paper>
            </Fade>
        </Box >
    )
}