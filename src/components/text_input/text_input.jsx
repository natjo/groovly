import React from 'react';
import TextField from '@material-ui/core/TextField';
import useLocalStorage from "react-use-localstorage";

export function TextInput(props) {
    // Textfield that allows any input
    // Props:
    // label: string, will be shown as the label of the field. Will also be used as part of the key for content persistence throught reloads
    // defaultValue: starting value of the field
    // valueRef: reference value that can be used from the parent component. Will be up to date with current value
    let local_storage_label = "text_input_" + props.label.replace(/\s/g, '_');
    const [value, setValue] = useLocalStorage(local_storage_label, props.defaultValue);

    function onChange(event) {
        const newValue = event.target.value;
        setValue(newValue);
        props.valueRef.current = newValue;
    }

    return (
        <TextField
            required
            value={value}
            label={props.label}
            onChange={onChange}
            variant="outlined"
        />
    );
}
