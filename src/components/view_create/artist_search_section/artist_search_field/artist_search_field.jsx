import React from 'react';
import { Grid, TextField, CircularProgress } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { getArtistSearchResults, getUserPlaylists, getUserId, getArtistsInPlaylist } from '../../../../spotify/api';
import { addArtistToLS } from '../../../../utils/local_storage_interaction';
import { cmpArtistRelevance, cmpStringSimilarityToKeyword } from '../../../../utils/similarity';
import constants from 'constants.js';

// Note:
// When picking an option, I reset the inputValue to clear the displayed text.
// Internally after this clearing, the option will be set to be the new text.
// This undos my inputValue clearing.
// As a solution, I set a flag when picking an option, which stops rendering text
// On the first user input after this, text will be shown again.


const useStyles = makeStyles(({ palette }) => ({
    artistIcon: {
        height: '3.5em',
        width: '3.5em',
    },
    artistName: {
        fontSize: '1.1em',
    },
    followers: {
        color: palette.grey[500],
        fontSize: '0.9em',
    },
}));

export function ArtistSearchField(props) {
    const classes = useStyles();
    const [loading, setLoading] = React.useState(false);
    const [options, setOptions] = React.useState([]);
    const [inputValue, setInputValue] = React.useState('');

    function updateOptions(searchString) {
        setLoading(true);
        let handleSearchResult = (result) => {
            setOptions(result);
            setLoading(false);
        }
        if (props.pickArtistsFromPlaylists) {
            getUserId(userId => {
                getUserPlaylists(userId, handleSearchResult);
            });
        } else {
            getArtistSearchResults(searchString, handleSearchResult);
        }
    }

    function renderOptionLabel(option) {
        if (props.pickArtistsFromPlaylists) {
            return <Grid>
                <span className={classes.artistName}>{option.name}</span>
                <span className={classes.followers}> - {option.numTracks} Tracks</span>
            </Grid>;
        }
        return <Grid container justify="flex-start" alignItems="center" spacing={1}>
            <Grid item>
                <img src={option.imageUrl} alt="artist-icon" className={classes.artistIcon} />
            </Grid>
            <Grid item>
                <span className={classes.artistName}>{option.name}</span>
            </Grid>
            <Grid item>
                <span className={classes.followers}> - {option.followers} Follower</span>
            </Grid>
        </Grid>;
    }

    // This empties the search field after selecting an item
    function getOptionLabel(option) {
        return '';
    }

    function filterOptions(options, { inputValue }) {
        if (props.pickArtistsFromPlaylists) {
            options.sort((a, b) => cmpStringSimilarityToKeyword(a.name, b.name, inputValue));
        } else {
            options.sort((a, b) => cmpArtistRelevance(a, b, inputValue));
        }
        return options;
    }

    function onChange(event, newValue) {
        if (newValue) {
            if (props.pickArtistsFromPlaylists) {
                getArtistsInPlaylist(newValue.id, (artists) => {

                    if (artists.length > constants.MAX_ARTISTS_FROM_PLAYLIST) {
                        artists = artists.sort(() => Math.random() - 0.5).slice(0, constants.MAX_ARTISTS_FROM_PLAYLIST);
                    }
                    artists.forEach(artist => {
                        addArtistToLS(artist);
                        props.updateArtists();
                    })
                });
            } else {
                addArtistToLS(newValue);
                props.updateArtists();
            }
            setInputValue('');
        }
    }


    return (
        <Autocomplete
            id="artist-picker"
            inputValue={inputValue}
            freeSolo
            autoHighlight
            fullWidth={true}
            filterOptions={filterOptions}
            onInputChange={(event, newInputValue) => {
                setInputValue(newInputValue);
                updateOptions(newInputValue);
            }}
            onChange={onChange}
            getOptionSelected={(option, value) => option.name === value.name}
            getOptionLabel={getOptionLabel}
            renderOption={renderOptionLabel}
            options={options}
            loading={loading}
            renderInput={(params) => (
                <TextField
                    {...params}
                    label={props.pickArtistsFromPlaylists ? "Search for Playlist" : "Search for Artist"}
                    variant="outlined"
                    InputProps={{
                        ...params.InputProps,
                        endAdornment: (
                            <React.Fragment>
                                {loading ? <CircularProgress color="inherit" size={20} /> : null}
                                {params.InputProps.endAdornment}
                            </React.Fragment>
                        ),
                    }}
                />
            )}
        />
    );
}
