import React from 'react';
import { Grid, Box, Divider, Switch, FormGroup, FormControlLabel } from '@material-ui/core';
import { ArtistSearchField } from './artist_search_field/artist_search_field';
import { SectionHeading } from 'components/section_heading/section_heading';
import { removeArtistsFromLS, removeGeneratedPlaylistFromLS } from 'utils/local_storage_interaction';
import { PickedArtistsList } from './picked_artists_list/picked_artists_list';
import { ButtonWithConfirmation } from 'components/button_with_confirmation/button_with_confirmation';
import { isNoArtistPicked } from 'spotify/other';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(({ palette }) => ({
    searchArea: {
        // minWidth: '300px',
        maxWidth: '800px',
    },
    darkBackground: {
        // background: `linear-gradient(${palette.secondary.light}, white)`
        // background: `linear-gradient(${palette.secondary.light}, white)`
        background: palette.secondary.light
    },
}));


export function ArtistSearchSection(props) {
    const classes = useStyles();

    const [pickArtistsFromPlaylists, setPickArtistsFromPlaylists] = React.useState(false);

    function handleRemovePickedArtistsClick() {
        removeArtistsFromLS();
        removeGeneratedPlaylistFromLS()
        props.artistSelectionChanged();
    }

    function switchArtistPickMethod() {
        setPickArtistsFromPlaylists(!pickArtistsFromPlaylists);
    }

    function RemovePickedArtistsButton() {
        if (isNoArtistPicked()) {
            return "";
        }
        return (
            <ButtonWithConfirmation item
                onClick={handleRemovePickedArtistsClick}
                dialogText={'Are you sure you want to remove all artists?'}
                alert
            >
                Remove Picked Artists
            </ButtonWithConfirmation>
        )
    }

    function ArtistSearchArea() {
        return (
            <Grid item container id="artist-search-area" justify="center" alignItems="stretch" className={classes.searchArea} spacing={2}>
                <Grid item xs={12} sm={8}>
                    <Box mx={6}>
                        <ArtistSearchField updateArtists={props.artistSelectionChanged} pickArtistsFromPlaylists={pickArtistsFromPlaylists} />
                    </Box>
                </Grid>
                <Grid item container justify="center" xs={12} sm={4}>
                    <FormGroup >
                        <FormControlLabel
                            control={
                                <Switch
                                    checked={pickArtistsFromPlaylists}
                                    onChange={switchArtistPickMethod}
                                />
                            }
                            label="Use Your Playlists"
                        />
                    </FormGroup>
                </Grid>
            </Grid>
        )
    }

    return (
        <Grid container direction="column" justify="flex-start" alignItems="stretch" spacing={3}>
            <Grid item>
                <SectionHeading title={"Select Artists"} />
            </Grid>
            <Grid item container justify="center" alignItems="center">
                <ArtistSearchArea />
            </Grid>
            <Grid item container justify="center" alignItems="center">
                <RemovePickedArtistsButton />
            </Grid>
            <Grid item>
                <Divider light />
            </Grid>
            <Grid item>
                <PickedArtistsList item artistsChanged updateArtists={props.artistSelectionChanged} />
            </Grid>
        </Grid >
    );
}