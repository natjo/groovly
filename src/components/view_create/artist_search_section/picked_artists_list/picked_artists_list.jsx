import React from 'react';
import Grid from '@material-ui/core/Grid';
import { ArtistCard } from 'components/artist_card/artist_card';
import { EmptyArtistCard } from 'components/artist_card/empty_artist_card';
import { getArtistsFromLS, removeArtistFromLS } from 'utils/local_storage_interaction';
import { isNoArtistPicked } from 'spotify/other';


export function PickedArtistsList(props) {
    function ListPickedArtists() {
        let pickedArtists = getArtistsFromLS();
        return pickedArtists.map(artist => {
            return (
                <Grid item xs={12} sm key={artist.name} >
                    <ArtistCard artist={artist} removePickedArtist={removePickedArtist} />
                </Grid>
            )
        });
    }

    function EmptyCard() {
        if (isNoArtistPicked()) {
            return "";
        }
        return (
            <Grid item xs={12} sm>
                <EmptyArtistCard text={"You can always add more artists!"} />
            </Grid>
        );
    }

    function removePickedArtist(artist) {
        removeArtistFromLS(artist);
        props.updateArtists();
    }


    return (
        <Grid container>
            <ListPickedArtists />
            <EmptyCard />
        </Grid>
    );
}
