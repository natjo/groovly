import React, { useRef } from 'react';
import { Grid } from '@material-ui/core';
import { SectionHeading } from 'components/section_heading/section_heading';
import { ButtonWithFinishDialog } from 'components/button_with_finish_dialog/button_with_finish_dialog';
import { TextInput } from 'components/text_input/text_input';
import { followGeneratedPlaylist } from 'spotify/other';
import constants from 'constants.js';

export function FollowPlaylistSection() {
    let newPlaylistNameRef = useRef(constants.DEFAULT_NEW_PLAYLIST_NAME);

    return (
        <Grid container direction="column" justify="center" alignItems="center" spacing={4}>
            <Grid item>
                <SectionHeading title={"Follow Playlist on Spotify"} />
            </Grid>
            <Grid item>
                <Grid container justify="center" alignItems="center" spacing={2}>
                    <Grid item>
                        <TextInput item xs={8}
                            label={'Name of New Playlist'}
                            defaultValue={constants.DEFAULT_NEW_PLAYLIST_NAME}
                            valueRef={newPlaylistNameRef}
                        ></TextInput>
                    </Grid>
                    <Grid item>
                        <ButtonWithFinishDialog item xs
                            onClick={(fun) => followGeneratedPlaylist(newPlaylistNameRef.current, fun)}
                            dialogText={`You follow your new playlist now. Enjoy discovering new artists!`}
                        >
                            Follow
            </ButtonWithFinishDialog>
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    )
}