import React from 'react';
import Grid from '@material-ui/core/Grid';
import { getGeneratedPlaylistFromLS, removeSongFromGeneratedPlaylistInLS } from 'utils/local_storage_interaction';
import { SongCard } from 'components/song_card/song_card';


export function GeneratedPlaylist(props) {
    function listItems() {
        let generatedPlaylist = getGeneratedPlaylistFromLS();
        return generatedPlaylist.map(song => {
            return (
                <Grid item xs={12} sm key={`${song.artist.name}-${song.name}`} >
                    <SongCard song={song} removeSong={removeSong} key={`${song.artist.name}-${song.name}`} />
                </Grid>
            )
        });
    }

    function removeSong(song) {
        removeSongFromGeneratedPlaylistInLS(song);
        props.updatePlaylist();
    }

    return (
        <Grid container >
            {listItems()}
        </Grid>
    );
}
