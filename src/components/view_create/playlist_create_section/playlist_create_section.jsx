import React, { useRef, useEffect } from 'react';
import { Grid, Divider, Box, LinearProgress, Typography, Collapse } from '@material-ui/core';
import { NumericalInput } from 'components/numerical_input/numerical_input';
import { AlertComponent } from 'components/alert_component/alert_component';
import { ButtonWithConfirmation } from 'components/button_with_confirmation/button_with_confirmation';
import { getArtistsFromLS, getGeneratedPlaylistFromLS, removeGeneratedPlaylistFromLS } from 'utils/local_storage_interaction';
import { generatePlaylist } from 'spotify/generate_playlist';
import { SectionHeading } from 'components/section_heading/section_heading';
import { GeneratedPlaylist } from 'components/view_create/generated_playlist/generated_playlist';
import constants from 'constants.js';
import { isNoPlaylistCreated } from 'spotify/other';
import { FollowPlaylistSection } from 'components/view_create/follow_playlist_section/follow_playlist_section';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(({ palette }) => ({
    PlaylistParamSection: {
        width: '60vw',
    },
    progressBar: {
        width: '80vw',
    },
}));


export function PlaylistCreateSection() {
    const classes = useStyles();

    // Just a dummy value that triggers rendering of the displayed playlist when changed
    const [playlistChanged, setPlaylistChanged] = React.useState(0);
    const [finishedPlaylistGeneration, setFinishedPlaylistGeneration] = React.useState(false);
    const [openAlert, setOpenAlert] = React.useState(false);
    const [alertText, setAlertText] = React.useState('');
    const [numSearchedArtists, setNumSearchedArtists] = React.useState(0);
    const [progressbarValue, setProgressbarValue] = React.useState(0);
    let numSongsInPlaylistRef = useRef(constants.DEFAULT_NUM_SONGS_IN_PLAYLIST);

    let maxNumFollowersRef = useRef(constants.DEFAULT_MAX_NUM_FOLLOWERS);

    useEffect(() => {
        setFinishedPlaylistGeneration(!isNoPlaylistCreated())
    }, []);

    function updatePlaylistSearchDisplay() {
        setPlaylistChanged(playlistChanged + Math.random());
    }


    function handleGeneratePlaylistRequest() {
        if (verifyParameters()) {
            removeGeneratedPlaylist();
            generatePlaylist(
                updatePlaylist,
                setNumSearchedArtists,
                maxNumFollowersRef.current,
                numSongsInPlaylistRef.current,
                () => setFinishedPlaylistGeneration(true)
            );
        } else {
            setOpenAlert(true);
        }
    }

    function removeGeneratedPlaylist() {
        removeGeneratedPlaylistFromLS();
        updatePlaylist();
        setFinishedPlaylistGeneration(false);
    }

    function updatePlaylist() {
        let generatedPlaylist = getGeneratedPlaylistFromLS();
        setProgressbarValue((generatedPlaylist.length / numSongsInPlaylistRef.current) * 100);
        updatePlaylistSearchDisplay();
    }


    function verifyParameters() {
        // If not all parameters work, this function will set the error message accordingly
        let pickedArtists = getArtistsFromLS();
        if (pickedArtists.length < constants.MIN_PICKED_ARTISTS) {
            setAlertText(`Please select at least ${constants.MIN_PICKED_ARTISTS} artists`);
            return false;
        }
        if (maxNumFollowersRef.current < constants.MIN_MAX_FOLLOWERS) {
            setAlertText(`Maximum followers needs to be at least ${constants.MIN_MAX_FOLLOWERS}`);
            return false;
        }
        if (numSongsInPlaylistRef.current < 1) {
            setAlertText(`Set at least 1 song for the generated playlist`);
            return false;
        }
        if (numSongsInPlaylistRef.current > constants.MAX_SONGS_IN_PLAYLIST) {
            setAlertText(`Too many songs for the generated playlist set`);
            return false;
        }
        setAlertText('');
        return true;
    }

    function RemoveGeneratedPlaylistButton() {
        if (isNoPlaylistCreated()) {
            return "";
        }
        return (
            <ButtonWithConfirmation item
                onClick={removeGeneratedPlaylist}
                dialogText={'Are you sure you want to remove the playlist?'}
                alert
            >
                Remove Generated Playlist
            </ButtonWithConfirmation>
        )
    }

    function GeneratePlaylistButton() {
        return (
            <ButtonWithConfirmation item
                onClick={handleGeneratePlaylistRequest}
                dialogText={'A playlist was already generated. Do you want to remove it and create a new playlist from your current selection of artists?'}
                checkShowDialog={() => !isNoPlaylistCreated()}
            >
                Create Playlist
            </ButtonWithConfirmation>
        )
    }

    function MaxFollowersInputField() {
        return (
            <NumericalInput item
                label={'Maximum followers'}
                defaultValue={constants.DEFAULT_MAX_NUM_FOLLOWERS}
                valueRef={maxNumFollowersRef}
            ></NumericalInput>
        );
    }

    function MaxNumSongsInPlaylistInputField() {
        return (
            <NumericalInput item
                label={'Songs in new playlist'}
                defaultValue={constants.DEFAULT_NUM_SONGS_IN_PLAYLIST}
                valueRef={numSongsInPlaylistRef}
            ></NumericalInput>
        );
    }

    function CreatedPlaylistHeading() {
        if (isNoPlaylistCreated() && numSearchedArtists === 0) {
            return "";
        }
        return (
            <Grid container justify="center" alignItems="center">
                <Grid item>
                    <LinearProgress variant="determinate" value={progressbarValue} />
                </Grid>
                <Grid item>
                    <NumSearchedArtistText />
                </Grid>
            </Grid>
        )
    }

    function NumSearchedArtistText() {
        if (numSearchedArtists === 0) {
            return "";
        }
        return (
            <Typography variant="body1" align="center">
                Evaluating {numSearchedArtists} artists
            </Typography>
        );
    }

    function PlaylistParamsSection() {
        return (
            <Grid
                item
                container
                direction="row"
                justify="center"
                alignItems="center"
                spacing={4}
                className={classes.PlaylistParamSection}
            >
                <Grid item xs={12} sm={4}>
                    <MaxFollowersInputField />
                </Grid>
                <Grid item xs={12} sm={4}>
                    <MaxNumSongsInPlaylistInputField />
                </Grid>
                <Grid item xs={12} sm={4} container justify="center" alignItems="center">
                    <GeneratePlaylistButton />
                </Grid>
            </Grid>
        )
    }


    return (
        <Grid container direction="column" justify="flex-start" alignItems="stretch" spacing={3}>
            <Box mb={2}>
                <SectionHeading title={"Generate Playlist"} />
            </Box>
            <Grid item container justify="center" alignItems="center">
                <PlaylistParamsSection />
            </Grid>
            <Grid item container justify="center" alignItems="center">
                <RemoveGeneratedPlaylistButton />
            </Grid>
            <Grid item container justify="center" alignItems="center">
                <LinearProgress variant="determinate" value={progressbarValue} className={classes.progressBar} />
            </Grid>
            <Grid item>
                <AlertComponent item open={openAlert} closeAlert={() => setOpenAlert(false)}>
                    {alertText}
                </AlertComponent>
                <CreatedPlaylistHeading></CreatedPlaylistHeading>
            </Grid>
            <GeneratedPlaylist updatePlaylist={updatePlaylist} />
            <Grid item>
                <Collapse in={finishedPlaylistGeneration}>
                    <Box mb={2}>
                        <Divider light />
                    </Box>
                    <FollowPlaylistSection />
                </Collapse>
            </Grid>
        </Grid>
    );
}