import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Grid } from '@material-ui/core';
import Collapse from '@material-ui/core/Collapse';
import { ArtistSearchSection } from './artist_search_section/artist_search_section';
import { PlaylistCreateSection } from './playlist_create_section/playlist_create_section';
import { isNoArtistPicked } from 'spotify/other';

const useStyles = makeStyles({
    root: {
        minHeight: '80vh',
    }
});


export function ViewCreate() {
    const classes = useStyles();

    const [artistsChanged, setArtistsChanged] = useState(0);
    const [shown, setShown] = useState(false);
    useEffect(() => {
        setShown(!isNoArtistPicked());
    }, []);

    function updateArtists() {
        setShown(!isNoArtistPicked());
        setArtistsChanged(artistsChanged + Math.random());
    }

    return (
        <Grid container direction="column" justify="flex-start" spacing={4} className={classes.root}>
            <Grid item>
                <ArtistSearchSection artistSelectionChanged={updateArtists} />
            </Grid>
            <Grid item>
                <Collapse in={shown}>
                    <PlaylistCreateSection />
                </Collapse>
            </Grid>
        </Grid >
    );
}
