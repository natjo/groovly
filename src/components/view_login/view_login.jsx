import React from "react";
import Typography from '@material-ui/core/Typography';
import { useTheme } from '@material-ui/core/styles';
import { Grid } from '@material-ui/core';
import { generateAccessTokenGenerationURL } from "../../spotify/auth";
import Button from '@material-ui/core/Button';
import ParticlesBg from 'particles-bg';


function Background() {
  const theme = useTheme();
  return <ParticlesBg type="cobweb" num={100} color={theme.palette.secondary.main} bg={true} />;
  // return <ParticlesBg type="lines" num={50} color={theme.palette.secondary.main} bg={true} />;
}


export function ViewLogin() {
  return (
    <Grid container>
      <Grid item container direction="column" justify="center" alignItems="center" spacing={5}>
        <Grid item>
          <Typography variant="h3" align="center">
            Discover Talented Musicians!
          </Typography>
        </Grid>
        <Grid item container direction="column" justify="center" alignItems="center">
          <Grid item>
            <Button variant="contained" color="primary" size="large" href={generateAccessTokenGenerationURL()}>Spotify Login</Button>
          </Grid>
          <Grid item>
            <Typography variant="caption" align="center">
              This App requires information from Spotify to fully function
          </Typography>
          </Grid>
        </Grid>
        <Grid item>
          <Typography variant="h5" align="center">
            Tell us about your favorite artists and we will generate a playlist of undiscovered musicians specially for you.
          </Typography>
        </Grid>
      </Grid>
      <Background />
    </Grid>
  );
}