module.exports = {
    DEFAULT_MAX_NUM_FOLLOWERS: 10000,
    DEFAULT_NUM_SONGS_IN_PLAYLIST: 20,
    DEFAULT_NEW_PLAYLIST_NAME: 'Discover New Horizons!',
    MIN_PICKED_ARTISTS: 1,
    MIN_MAX_FOLLOWERS: 500,
    MAX_SONGS_IN_PLAYLIST: 200,
    // WEBPAGE: 'https://groovly.app',
    WEBPAGE: 'http://localhost:3000',
    SOURCE_CODE_PAGE: "https://gitlab.com/natjo/Groovly",
    SPOTIFY_CLIENT_ID: '40c61231f1d542c7807d9531c0503f9c', // This app's client id
    SPOTIFY_SCOPE: 'user-read-private user-read-email playlist-modify-public playlist-modify-private playlist-read-collaborative playlist-read-private',
    MAX_ARTISTS_FROM_PLAYLIST: 18,
}
