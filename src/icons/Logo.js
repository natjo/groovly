import * as React from 'react';

function SvgLogo(props) {
    return (
        <svg width={695.714} height={695.714} viewBox="0 0 184.074 184.074" {...props}>
            <g transform="translate(-4.06 -13.851)">
                <path fill="#86dfff" d="M4.06 13.851h184.074v184.074H4.06z" />
                <path
                    d="M138.459 141.398a47.345 47.345 0 01-56.304 28.38 47.345 47.345 0 01-34.572-52.73A47.345 47.345 0 0196.061 76.73"
                    fill="none"
                    stroke="#fff"
                    strokeWidth={8.83}
                    strokeLinecap="round"
                />
                <path
                    d="M125.004 136.284a32.952 32.952 0 01-39.715 19.427 32.952 32.952 0 01-23.299-37.576A32.952 32.952 0 0197.051 91.2"
                    fill="none"
                    stroke="#fff"
                    strokeWidth={8.83}
                    strokeLinecap="round"
                />
                <circle cx={94.408} cy={124.046} r={19.011} fill="none" stroke="#fff" strokeWidth={8.83} />
                <path
                    d="M113.42 123.55V39.69M145.09 71.36l-31.67-31.67"
                    fill="none"
                    stroke="#fff"
                    strokeWidth={8.83}
                    strokeLinecap="round"
                />
            </g>
        </svg>
    );
}

export default SvgLogo;
