import * as React from 'react';

function SvgLogoNoBackground(props) {
    return (
        <svg width={404.045} height={531.386} viewBox="0 0 106.904 140.596" {...props}>
            <g transform="translate(317.646 41.226)" fill="none" stroke="#fff" strokeWidth={8.83}>
                <path
                    d="M-221.833 64.959a47.345 47.345 0 01-56.304 28.38 47.345 47.345 0 01-34.571-52.73A47.345 47.345 0 01-264.231.291"
                    strokeLinecap="round"
                />
                <path
                    d="M-235.288 59.844a32.952 32.952 0 01-39.715 19.428 32.952 32.952 0 01-23.298-37.576 32.952 32.952 0 0135.06-26.935"
                    strokeLinecap="round"
                />
                <circle cx={-265.883} cy={47.607} r={19.011} />
                <path d="M-246.872 47.111v-83.86M-215.202-5.078l-31.67-31.67" strokeLinecap="round" />
            </g>
        </svg>
    );
}

export default SvgLogoNoBackground;
