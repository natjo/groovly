import { getLocalSpotifyAccessToken, hasTokenExpired, removeExpiredSpotifyAccessToken } from './auth';
import { forceReloadPage } from '../utils/general';

function getMe() {
    let url = new URL('https://api.spotify.com/v1/me');
    baseApiCall(url, console.log);
}

function getArtistSearchResults(artistName, fun) {
    if (artistName.length === 0) {
        return fun([]);
    }
    let filter = (data) => {
        // Get only relevant info
        let filteredData = data['artists']['items'];
        filteredData = filteredData.map((x) => {
            let imageUrl = 'default_image';
            if (x['images'].length > 0) {
                imageUrl = x['images'][0]['url'];
            }
            return {
                name: x['name'],
                id: x['id'],
                followers: x['followers']['total'],
                popularity: x['popularity'],
                imageUrl: imageUrl,
            };
        });

        // Some artists appear multiple times, with different number of followers
        // Keep only the follower with the largest number of followers
        let followersOfArtists = {};
        filteredData.forEach((artist) => {
            if (artist.name in followersOfArtists) {
                followersOfArtists[artist.name] = Math.max(followersOfArtists[artist.name], artist.followers);
            } else {
                followersOfArtists[artist.name] = artist.followers;
            }
        });

        filteredData = filteredData.filter((artist) => artist.followers >= followersOfArtists[artist.name]);

        return filteredData;
    };

    let params = {
        q: artistName,
        type: 'artist',
        limit: 20, // Maximum number of results from query, max=50
    };

    let url = new URL('https://api.spotify.com/v1/search');
    baseApiCall(url, fun, filter, params);
}

// id is the ID of the artist
function getRelatedArtists(id, fun = null) {
    let filter = (data) => {
        let filteredData = data['artists'];
        // That mapping is kind of unnecessary, since the original array has the same structure
        filteredData = filteredData.map((x) => {
            let imageUrl = 'default_image';
            if (x['images'].length > 0) {
                imageUrl = x['images'][0]['url'];
            }
            return {
                name: x['name'],
                id: x['id'],
                popularity: x['popularity'],
                followers: x['followers']['total'],
                imageUrl: imageUrl,
            };
        });
        return filteredData;
    };

    let url = new URL(`https://api.spotify.com/v1/artists/${id}/related-artists`);
    baseApiCall(url, fun, filter);
}

// id is the ID of the artist
function getTopTracks(id, country, fun = null) {
    let filter = (data) => {
        let filteredData = data['tracks'];
        // That mapping is kind of unnecessary, since the original array has the same structure
        filteredData = filteredData.map((x) => {
            return {
                id: x['id'],
                name: x['name'],
                songUri: x['uri'],
                popularity: x['popularity'],
                imageUrl: x['album']['images'][0]['url'],
            };
        });
        return filteredData;
    };

    let url = new URL(`https://api.spotify.com/v1/artists/${id}/top-tracks`);

    let params = {
        country: country,
    };

    baseApiCall(url, fun, filter, params);
}

function getUserId(fun = null) {
    let filter = (data) => {
        return data['id'];
    };

    let url = new URL(`https://api.spotify.com/v1/me`);

    baseApiCall(url, fun, filter);
}

function createPlaylist(user_id, name, description, fun = null) {
    let filter = (data) => {
        return data['id'];
    };

    let url = new URL(`https://api.spotify.com/v1/users/${user_id}/playlists`);

    let params = {
        name: name,
        description: description,
    };

    let method = 'POST';

    baseApiCall(url, fun, filter, params, method);
}

function addTracksToPlaylist(playlistId, songUris, fun = null) {
    let filter = (data) => {
        return data;
    };

    let url = new URL(`https://api.spotify.com/v1/playlists/${playlistId}/tracks`);

    let params = {
        uris: songUris,
    };

    let method = 'POST';

    baseApiCall(url, fun, filter, params, method);
}

function getArtistsInfo(artistIds, fun) {
    let filter = (data) => {
        let filteredData = data['artists'];
        filteredData = filteredData.map((x) => {
            let imageUrl = 'default_image';
            if (x['images'].length > 0) {
                imageUrl = x['images'][0]['url'];
            }
            return {
                name: x['name'],
                id: x['id'],
                followers: x['followers']['total'],
                popularity: x['popularity'],
                imageUrl: imageUrl,
            };
        });

        // // Some artists appear multiple times, with different number of followers
        // // Keep only the follower with the largest number of followers
        // let followersOfArtists = {};
        // filteredData.forEach((artist) => {
        //     if (artist.name in followersOfArtists) {
        //         followersOfArtists[artist.name] = Math.max(followersOfArtists[artist.name], artist.followers);
        //     } else {
        //         followersOfArtists[artist.name] = artist.followers;
        //     }
        // });

        // filteredData = filteredData.filter((artist) => artist.followers >= followersOfArtists[artist.name]);

        return filteredData;
    };

    let params = {
        ids: artistIds.join(','),
    };

    let url = new URL(`https://api.spotify.com/v1/artists`);

    baseApiCall(url, fun, filter, params);
}

function getUserPlaylists(userId, fun) {
    getNextPageOfPlaylists(userId, [], fun);
}

// We can only get 50 at a time, so this function calls itself until it has all playlists, 
// at which point it will call the original follow up function
function getNextPageOfPlaylists(userId, prevFetchedPlaylists, fun) {
    let filter = (data) => {
        let filteredData = data['items'];
        filteredData = filteredData.map((x) => {
            return {
                id: x['id'],
                name: x['name'],
                numTracks: x['tracks']['total']
            }
        });
        return { totalNumPlaylists: data['total'], playlists: filteredData, };
    };

    // This function will call itself until all playlists are found
    let recursiveFun = (data) => {
        prevFetchedPlaylists.push(...data.playlists);
        if (data.totalNumPlaylists > prevFetchedPlaylists.length) {
            getNextPageOfPlaylists(userId, prevFetchedPlaylists, fun);
        } else {
            prevFetchedPlaylists = prevFetchedPlaylists.filter(x => x.numTracks > 0);
            fun(prevFetchedPlaylists)
        }
    }

    let url = new URL(`https://api.spotify.com/v1/users/${userId}/playlists`);

    let params = {
        limit: 50, // Maximum
        offset: prevFetchedPlaylists.length
    };

    let method = 'GET';

    baseApiCall(url, recursiveFun, filter, params, method);
}

function getArtistsInPlaylist(playlistId, fun = null) {
    getSongsInPlaylist(playlistId, (songs) => {
        let artists = [];
        songs.forEach((song) => {
            song['artists'].forEach((artist) => {
                if (!isArtistInArtistList(artists, artist)) {
                    artists.push(artist);
                }
            })
        });
        completeArtistsInfo(artists, fun);
    });
}
function completeArtistsInfo(initArtists, fun) {
    completeNextArtistsInfos(initArtists, [], fun);
}

function completeNextArtistsInfos(initArtists, completedArtists, fun) {
    if (initArtists.length > 0) {
        let artistSearchIds = [];
        while (initArtists.length > 0 && artistSearchIds.length < 10) {
            artistSearchIds.push(initArtists.pop().id);
        }
        getArtistsInfo(artistSearchIds, (artistInfos) => {
            artistInfos.forEach(artistInfo => {
                completedArtists.push(artistInfo);
            });
            completeNextArtistsInfos(initArtists, completedArtists, fun);
        });
    } else {
        fun(completedArtists);
    }
}

function isArtistInArtistList(artistList, newArtist) {
    return artistList.some((artist) => artist.id === newArtist.id);
}

// Currently only gets the first 100 songs of a playlist due to paging
function getSongsInPlaylist(playlistId, fun = null) {
    getNextPageOfSongsInPlaylists(playlistId, [], fun);
}

function getNextPageOfSongsInPlaylists(playlistId, prevFetchedSongs, fun) {
    let filter = (data) => {
        let filteredData = data['items'];
        filteredData = filteredData.map((x) => {
            return x['track'];
        });
        return { totalNumSongs: data['total'], songs: filteredData };
    };

    // This function will call itself until all playlists are found
    let recursiveFun = (data) => {
        prevFetchedSongs.push(...data.songs);
        if (data.totalSongs > prevFetchedSongs.length) {
            getNextPageOfPlaylists(playlistId, prevFetchedSongs, fun);
        } else {
            fun(prevFetchedSongs);
        }
    }

    let url = new URL(`https://api.spotify.com/v1/playlists/${playlistId}/tracks`);

    let params = {
        limit: 100, // Maximum
        offset: prevFetchedSongs.length
    };

    let method = 'GET';

    baseApiCall(url, recursiveFun, filter, params, method);
}

// Filter is used to filter from the verbose response only the relevant information
function baseApiCall(url, fun, filter = null, params = null, method = 'GET') {
    if (filter == null) {
        // By default do nothing, just return the original object
        filter = (response) => {
            return response;
        };
    }
    // default_val will be used if the http call fails. This is a workaround due to the shitty, inconsistent Spotify API
    let default_val = [{}];
    url = updateUrlWithQueryParams(url, method, params);
    const requestOptions = getRequestOptions(method, params);
    fetch(url, requestOptions)
        .then(handleErrors)
        .then((response) => response.json())
        .then((json) => fun(filter(json)))
        .catch((e) => {
            console.log(`Error when calling URL ${url}\n${e}`);
            fun(default_val);
        }
        );
}

function updateUrlWithQueryParams(url, method, params) {
    if (method === 'GET' && params !== null) {
        Object.keys(params).forEach((key) => url.searchParams.append(key, params[key]));
    }
    return url;
}

// POST and GET request require structurely different options
function getRequestOptions(method, params) {
    if (hasTokenExpired()) {
        removeExpiredSpotifyAccessToken();
        alert("Your session has timed out. Please login again!");
        forceReloadPage();
    }
    let requestOptions = {
        method: method,
        headers: {
            Authorization: 'Bearer ' + getLocalSpotifyAccessToken(),
            'Content-Type': 'application/json',
        },
    };
    if (method === 'POST') {
        requestOptions.body = JSON.stringify(params);
        requestOptions.json = true;
    }
    return requestOptions;
}

function handleErrors(response) {
    if (!response.ok) {
        console.log(response);
        response.json().then((json) => {
            console.log(
                `Error during api call:\n ${JSON.stringify(response.url)}\n ${JSON.stringify(response.statusText)}`
            );
            console.log(json.error);
        });
        throw new Error(
            `Error during api call:\n${JSON.stringify(response.url)}\n${JSON.stringify(response.statusText)}`
        );
    }
    return response;
}

export {
    getMe,
    getArtistSearchResults,
    getRelatedArtists,
    getTopTracks,
    getUserId,
    createPlaylist,
    addTracksToPlaylist,
    getUserPlaylists,
    getArtistsInPlaylist,
    getArtistsInfo,
};
