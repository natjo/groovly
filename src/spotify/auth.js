import { generateRandomString } from '../utils/general';
import {
    getSpotifyAccessTokenFromLS,
    setSpotifyAccessTokenInLS,
    removeSpotifyAccessTokenFromLS,
    getSpotifyAccessStateFromLS,
    setSpotifyAccessStateInLS,
    removeSpotifyAccessStateFromLS,
    getSecondsUntilExpirationSpotifyTokenFromLS,
    setSecondsUntilExpirationSpotifyTokenInLS,
    removeSecondsUntilExpirationSpotifytokenFromLS,
} from '../utils/local_storage_interaction';
import constants from 'constants.js';

// Creates the url to call for a new spotify access token
// Additionally creates a short string in the local storage, that will be returned with the token
// This validates the response to ensure that both the request and response originated in the same browser.
// This provides protection against attacks such as cross-site request forgery
function generateAccessTokenGenerationURL() {
    let state = generateRandomString(16);
    setSpotifyAccessStateInLS(state);

    let url = 'https://accounts.spotify.com/authorize';
    url += '?response_type=token';
    url += '&client_id=' + encodeURIComponent(constants.SPOTIFY_CLIENT_ID);
    url += '&scope=' + encodeURIComponent(constants.SPOTIFY_SCOPE);
    url += '&redirect_uri=' + encodeURIComponent(constants.WEBPAGE);
    url += '&state=' + encodeURIComponent(state);
    return url;
}

function removeExpiredSpotifyAccessToken() {
    if (hasTokenExpired()) {
        removeSpotifyAccessStateFromLS();
        removeSpotifyAccessTokenFromLS();
        removeSecondsUntilExpirationSpotifytokenFromLS();
    }
}

function existsValidSpotifyAccessToken() {
    if (getSpotifyAccessTokenFromLS() !== null) {
        return !hasTokenExpired();
    }
    return false;
}

// Expiration date is saved as seconds since 1970
// Check if current time is after expiration time
function hasTokenExpired() {
    const secondsUntilExpiration = getSecondsUntilExpirationSpotifyTokenFromLS();
    if (secondsUntilExpiration === null) {
        return true;
    }
    const now = new Date();
    const secondsSinceEpoch = Math.round(now.getTime() / 1000);
    return secondsSinceEpoch > secondsUntilExpiration;
}

// Returns a valid spotify auth token in the local storage
function getLocalSpotifyAccessToken() {
    return getSpotifyAccessTokenFromLS();
}

function saveAccessTokenFromHashParams(response) {
    setSpotifyAccessTokenInLS(response.access_token);
    saveExpirationDate(response.expires_in);
    removeSpotifyAccessStateFromLS();
    console.log('Spotify access token and expiration date saved');
}

// Checks if the by Spotify returned message is valid by comparing state strings
function isValidResponse(response) {
    let originalState = getSpotifyAccessStateFromLS();
    return response.state === originalState;
}

function saveExpirationDate(secondsValid) {
    const now = new Date();
    const secondsSinceEpoch = Math.round(now.getTime() / 1000);
    const secondsUntilExpiration = secondsSinceEpoch + parseInt(secondsValid);
    setSecondsUntilExpirationSpotifyTokenInLS(secondsUntilExpiration);
}

export {
    existsValidSpotifyAccessToken,
    getLocalSpotifyAccessToken,
    saveAccessTokenFromHashParams,
    generateAccessTokenGenerationURL,
    isValidResponse,
    removeExpiredSpotifyAccessToken,
    hasTokenExpired,
};
