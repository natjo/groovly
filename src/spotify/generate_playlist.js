import {
    getArtistsFromLS,
    getGeneratedPlaylistFromLS,
    addSongToGeneratedPlaylistInLS,
} from '../utils/local_storage_interaction';
import { getRelatedArtists, getTopTracks } from './api';
import { getRandomArrayElement, getRandomInt, shuffle } from '../utils/general';

const MAX_ARTISTS_BEFORE_FAIL = 5000;
const RANDOM_PICK_INFLUENCE = 40;  // Value should be in range 0 to 100
const MIN_NUM_ARTIST_PREFETCH = 40; // Fetches this many artists times total num artists before picking songs

// Main idea: iterate through list of selected artists, pick one song from a similar, unpopular band,
// add unpopular band to list iterated through in case there were not enough artists picked to fill the generated playlist
function generatePlaylist(updateDisplayedPlaylist, setNumSearchedArtists, maxFollowers, numSongsInPlaylist, functionOnFinish) {
    let pickedArtists = getArtistsFromLS();
    let artistsToIterate = pickedArtists.map((x) => [x]);
    shuffle(artistsToIterate);
    let options = {
        maxFollowers: maxFollowers,
        numSongsInPlaylist: numSongsInPlaylist,
        updateDisplayedPlaylist: updateDisplayedPlaylist,
        setNumSearchedArtists: setNumSearchedArtists,
        functionOnFinish: functionOnFinish,
    };
    addSongRecursively(artistsToIterate, 0, options);
}

// artistToIterate is a list of lists, each inner list represents the related artist of one initially picked artist
// idx is the index of the inner list, which a new song is currently picked from
// This function updated the generated playlist automatically in the local storage while running
function addSongRecursively(artistsToIterate, idx, options) {
    let generatedPlaylist = getGeneratedPlaylistFromLS();
    // Check for a stop condition
    if (generatedPlaylist.length >= options.numSongsInPlaylist) {
        options.functionOnFinish();
        return;
    }

    if (artistsToIterate[idx].length > MAX_ARTISTS_BEFORE_FAIL) {
        alert(
            'Not enough artists found with the picked set of artists. We are sorry about that. Please change your selection.'
        );
        options.functionOnFinish();
        return;
    }

    let evalArtist = popArtistToEval(artistsToIterate[idx], options.maxFollowers);

    // Add related artists
    getRelatedArtists(evalArtist.id, (artists) => {
        artists = excludeArtistsInUse(artists, artistsToIterate[idx]);
        artistsToIterate[idx].push(...artists);

        let numSearchedArtists = artistsToIterate.reduce((count, row) => count + row.length, 0);
        options.setNumSearchedArtists(numSearchedArtists);

        // Check if enough artists where prefetched
        if (numSearchedArtists < MIN_NUM_ARTIST_PREFETCH * Math.sqrt(artistsToIterate.length) * 3) {
            artistsToIterate[idx].push(evalArtist);
            // Run recursively
            idx = ++idx % artistsToIterate.length;
            addSongRecursively(artistsToIterate, idx, options);

            // Check if the artist has not too many followers
        } else if (evalArtist.followers < options.maxFollowers) {
            // console.log( `Get song number ${generatedPlaylist.length} for artist ${evalArtist['name']} with ${evalArtist['followers']} followers and popularity ${evalArtist['popularity']}`);
            let countryOfTopTracks = 'DE';
            getTopTracks(evalArtist.id, countryOfTopTracks, (tracks) => {
                let song = pickRandomSong(tracks, evalArtist);

                // Add song to local storage
                addSongToGeneratedPlaylistInLS(song);
                options.updateDisplayedPlaylist();

                // Run recursively
                idx = ++idx % artistsToIterate.length;
                setTimeout(() => {
                    addSongRecursively(artistsToIterate, idx, options);
                }, 250)
            });

            // If nothing works, repeat as above
        } else {
            // console.log(`Artist ${evalArtist['name']} has too many followers: ${evalArtist['followers']} and popularity ${evalArtist['popularity']}`);
            addSongRecursively(artistsToIterate, idx, options);
        }
    });
}

// Get new artist to find related songs,
// sort this to simply pick the best fitting one
function popArtistToEval(artistList, maxFollowers) {
    artistList.sort((a, b) => cmpArtists(a, b, maxFollowers));
    // artistList.forEach(x => console.log(`${x['popularity']}, ${x['followers']}, ${x['name']}, ${calcArtistValue(x)}`))
    return artistList.pop();
}

// Shift artists with a small follower number to the end to use first
// Artists with less than maxFollowers are sorted by popularity
function cmpArtists(a, b, maxFollowers) {
    if (a.followers < maxFollowers && b.followers < maxFollowers) {
        // This value without the randomness will be between -100 and 100
        return a.popularity - b.popularity + getRandomInt(-1, 1) * RANDOM_PICK_INFLUENCE;
    } else if (a.followers < maxFollowers) {
        return 1;
    } else if (b.followers < maxFollowers) {
        return -1;
    } else {
        return calcArtistValue(b) - calcArtistValue(a);
    }
}

function calcArtistValue(artist) {
    // +1 since popularity can be 0
    return artist.followers / (1 + artist.popularity);
}

function excludeArtistsInUse(artists, artistsToIterate) {
    let pickedArtists = getArtistsFromLS();
    let generatedPlaylist = getGeneratedPlaylistFromLS();
    return artists.filter((artist) => {
        return (
            !(
                isArtistInArtistList(artistsToIterate, artist)
                || isArtistInArtistList(pickedArtists, artist)
                || isArtistInPlaylist(generatedPlaylist, artist)
            )
        );
    });
}

function isArtistInPlaylist(playlist, artist) {
    return playlist.some((song) => song.artist.id === artist.id);
}

function isArtistInArtistList(artistList, evalArtist) {
    return artistList.some((artist) => artist.id === evalArtist.id);
}

function pickRandomSong(songs, artist) {
    let song = getRandomArrayElement(songs);
    // console.log(`Song picked: ${artist.name} - ${song.name}`);
    song.artist = {};
    song.artist.id = artist.id;
    song.artist.name = artist.name;
    song.artist.followers = artist.followers;
    return song;
}

export { generatePlaylist };
