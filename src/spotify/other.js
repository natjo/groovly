import { getGeneratedPlaylistFromLS, getArtistsFromLS } from '../utils/local_storage_interaction';
import { getUserId, createPlaylist, addTracksToPlaylist } from './api';
import constants from 'constants.js';


function generatePlaylistDescription() {
    const selectedArtists = getArtistsFromLS();
    const artistsString = selectedArtists.map((x) => x.name).join(', ');
    return `Auto generated from your favourite artists! Find new music similar to ${artistsString}. Visit ${constants.WEBPAGE} to explore more small artists!`;
}

function followGeneratedPlaylist(playlistName, fun_on_success) {
    // Check if a playlist was already created
    let generatedPlaylist = getGeneratedPlaylistFromLS();
    if (generatedPlaylist.length === 0) {
        return;
    }

    getUserId((id) => {
        const description = generatePlaylistDescription();
        createPlaylist(id, playlistName, description, (playlistId) => {
            let generatedPlaylist = getGeneratedPlaylistFromLS();
            let songUris = generatedPlaylist.map((x) => x.songUri);
            addTracksToPlaylist(playlistId, songUris, fun_on_success);
        });
    });
}

function isNoArtistPicked() {
    let pickedArtists = getArtistsFromLS();
    return pickedArtists.length === 0;
}

function isNoPlaylistCreated() {
    let generatedPlaylist = getGeneratedPlaylistFromLS();
    return generatedPlaylist.length === 0;
}

export { followGeneratedPlaylist, isNoArtistPicked, isNoPlaylistCreated };
