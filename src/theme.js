import { createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
    palette: {
        primary: {
            main: "#C5FFA1",
        },
        secondary: {
            main: "#86DFFF",
            contrastText: "#ffffff",
        },
        alert: {
            main: "#FFD594",
        },
        darkBackground: {
            main: "#82A6B3",
        },
        tonalOffset: 0.3,
    },
    typography: {
        fontFamily: [
            'Ubuntu',
            'Roboto',
            '-apple-system',
            'BlinkMacSystemFont',
            '"Segoe UI"',
            '"Helvetica Neue"',
            'Arial',
            'sans-serif',
            '"Apple Color Emoji"',
            '"Segoe UI Emoji"',
            '"Segoe UI Symbol"',
        ].join(','),
    }
});

export default theme;