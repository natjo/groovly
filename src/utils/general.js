/**
 * Generates a random string containing numbers and letters
 * @param  {number} length The length of the string
 * @return {string} The generated string
 */
function generateRandomString(length) {
    let text = '';
    let possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

    for (let i = 0; i < length; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
}

// Obtains parameters from the hash of the URL
function getHashParams() {
    var hashParams = {};
    var e,
        r = /([^&;=]+)=?([^&;]*)/g,
        q = window.location.hash.substring(1);
    e = r.exec(q);
    while (e) {
        hashParams[e[1]] = decodeURIComponent(e[2]);
        e = r.exec(q);
    }
    return hashParams;
}

function cleanUrl() {
    window.history.pushState({}, document.title, "/");
}

function getRandomArrayElement(arr) {
    if (arr && arr.length) {
        return arr[Math.floor(Math.random() * arr.length)];
    }
    // The undefined will be returned if the empty array was passed
}

function forceReloadPage() {
    window.location.reload(false);
}

function getNumFollowersFormatted(artist) {
    return artist.followers.toLocaleString();
}

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
function getRandomFloat(min, max) {
    return Math.random() * (max - min) + min;
}

function shuffle(array) {
  array.sort(() => Math.random() - 0.5);
}

export { generateRandomString, getHashParams, cleanUrl, getRandomArrayElement, forceReloadPage, getNumFollowersFormatted, getRandomInt, getRandomFloat, shuffle };
