import ls from 'local-storage';

const LS_KEY_PICKED_ARTISTS = 'PICKED_ARTISTS';
const LS_KEY_GENERATED_PLAYLIST = 'GENERATED_PLAYLIST';
const LS_KEY_SPOTIFY_ACCESS_TOKEN = 'SPOTIFY_ACCESS_TOKEN';
const LS_KEY_SPOTIFY_ACCESS_STATE = 'SPOTIFY_ACCESS_STATE';
const LS_KEY_SECONDS_UNTIL_EXPIRATION = 'SECONDS_UNTIL_EXPIRATION';

function existsSpotifyAccessTokenInLS() {
    let val = ls.get(LS_KEY_SPOTIFY_ACCESS_TOKEN);
    return val !== null;
}

function setSpotifyAccessTokenInLS(token) {
    return ls.set(LS_KEY_SPOTIFY_ACCESS_TOKEN, token);
}

function getSpotifyAccessTokenFromLS() {
    return ls.get(LS_KEY_SPOTIFY_ACCESS_TOKEN);
}

function removeSpotifyAccessTokenFromLS() {
    return ls.remove(LS_KEY_SPOTIFY_ACCESS_TOKEN);
}

function getSpotifyAccessStateFromLS() {
    return ls.get(LS_KEY_SPOTIFY_ACCESS_STATE);
}

function setSpotifyAccessStateInLS(state) {
    return ls.set(LS_KEY_SPOTIFY_ACCESS_STATE, state);
}

function removeSpotifyAccessStateFromLS() {
    return ls.remove(LS_KEY_SPOTIFY_ACCESS_STATE);
}

function getSecondsUntilExpirationSpotifyTokenFromLS() {
    return ls.get(LS_KEY_SECONDS_UNTIL_EXPIRATION);
}

function setSecondsUntilExpirationSpotifyTokenInLS(seconds) {
    return ls.set(LS_KEY_SECONDS_UNTIL_EXPIRATION, seconds);
}

function removeSecondsUntilExpirationSpotifytokenFromLS() {
    return ls.remove(LS_KEY_SECONDS_UNTIL_EXPIRATION);
}

function addArtistToLS(newArtist) {
    let pickedArtists = getArtistsFromLS();
    if (!listIncludesArtist(pickedArtists, newArtist)) {
        pickedArtists.push(newArtist);
        ls.set(LS_KEY_PICKED_ARTISTS, pickedArtists);
    }
}

function removeArtistFromLS(removeArtist) {
    let pickedArtists = getArtistsFromLS();
    pickedArtists = pickedArtists.filter((a) => a.id !== removeArtist.id);
    ls.set(LS_KEY_PICKED_ARTISTS, pickedArtists);
}

function getArtistsFromLS() {
    return lsGetOrDefault(LS_KEY_PICKED_ARTISTS, []);
}

function removeArtistsFromLS() {
    ls.remove(LS_KEY_PICKED_ARTISTS);
}

function addSongToGeneratedPlaylistInLS(newSong) {
    let generatedPlaylist = getGeneratedPlaylistFromLS();
    generatedPlaylist.unshift(newSong);
    ls.set(LS_KEY_GENERATED_PLAYLIST, generatedPlaylist);
}

function removeSongFromGeneratedPlaylistInLS(removeSong) {
    let generatedPlaylist = getGeneratedPlaylistFromLS();
    generatedPlaylist = generatedPlaylist.filter((s) => s.id !== removeSong.id);
    ls.set(LS_KEY_GENERATED_PLAYLIST, generatedPlaylist);
}

function getGeneratedPlaylistFromLS() {
    return lsGetOrDefault(LS_KEY_GENERATED_PLAYLIST, []);
}

function removeGeneratedPlaylistFromLS() {
    ls.remove(LS_KEY_GENERATED_PLAYLIST);
}

function lsGetOrDefault(key, defaultVal) {
    let val = ls.get(key);
    if (!val) {
        return defaultVal;
    }
    return val;
}

function listIncludesArtist(artists, newArtist) {
    return artists.some((artist) => artist.name === newArtist.name);
}

export {
    addArtistToLS,
    getArtistsFromLS,
    removeArtistsFromLS,
    removeArtistFromLS,
    getGeneratedPlaylistFromLS,
    addSongToGeneratedPlaylistInLS,
    removeSongFromGeneratedPlaylistInLS,
    removeGeneratedPlaylistFromLS,
    existsSpotifyAccessTokenInLS,
    getSpotifyAccessTokenFromLS,
    setSpotifyAccessTokenInLS,
    removeSpotifyAccessTokenFromLS,
    getSpotifyAccessStateFromLS,
    setSpotifyAccessStateInLS,
    removeSpotifyAccessStateFromLS,
    getSecondsUntilExpirationSpotifyTokenFromLS,
    setSecondsUntilExpirationSpotifyTokenInLS,
    removeSecondsUntilExpirationSpotifytokenFromLS,
};
